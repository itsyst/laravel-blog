@extends('layout')
@section('content')
<div id="wrapper">
    <div id="page" class="container">
        <div id="content">
            <div class="title">
                <h2>{{$article->title}}</h2>
                <p>{{$article->body}}</p>




                <p>
                        <img src="/images/gtn.jpg"
                        alt=""
                        class="image image-full" />
                </p>

                    {!! $article->body!!}
                    <p style="margin-top:1.4em;font-weight:700">
                        @foreach ($article->tags as $tag)
                           <a href="/articles?tag={{$tag->name}}">{{$tag->name}}
                           </a>
                        @endforeach
                    </p>
             </div>
        </div>
    </div>
</div>
@endsection()
