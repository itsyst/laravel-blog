@extends('layout')
@section('content')
<div id="wrapper">
    <div id="page" class="container">
            <h1 class="heading">New article</h1>
            <form method="POST" action="/articles" >
                @csrf
                <div class="field">
                    <label class="label" for="title">Title:</label>

                    <div class="control">
                        <input
                        class="input"
                        type="text"
                        name="title"
                        id="title"
                        value="{{old('title')}}"
                        class="@error('title') is-invalid @enderror">
                     </div>
                    @error('title')
                    <p class="help is-danger">{{ $errors->first('title') }}</p>
                    @enderror

                </div>
                <div class="field">
                    <label class="label" for="exerpt">Excerpt:</label>

                    <div class="control">
                    <textarea
                    class="textarea"
                    name="exerpt"
                    id="exerpt"
                    value="{{old('exerpt')}}"
                    class="@error('exerpt') is-invalid @enderror"></textarea>

                    </div>
                    @error('exerpt')
                    <p class="alert alert-danger">{{ $errors->first('exerpt') }}</p>
                    @enderror
                </div>

                <div class="field">
                    <label class="label" for="body">Body:</label>

                    <div class="control">
                        <textarea
                        class="textarea"
                        name="body"
                        value="{{old('body')}}"
                        id="body"></textarea>
                    </div>
                    @error('body')
                    <p class="alert alert-danger">{{ $errors->first('body') }}</p>
                    @enderror
                </div>


                <div class="field">
                    <label class="label" for="title">Tags:</label>

                    <div class="control">
                        <select
                             multiple
                            type="text"
                            name="tags[]"
                            id="tags"
                            value="{{old('tags')}}"
                            class="@error('tags') is-invalid @enderror">
                        @foreach ($tags as $tag)
                       <option value="{{$tag->id}}">{{$tag->name}}</option>
                        @endforeach
                        </select>
                     </div>
                    @error('tags')
                    <p class="help is-danger">{{ $message }}</p>
                    @enderror

                </div>






                <div class="field is-grouped">
                    <div class="control">
                        <button class="button is-link" type="submit">Submit</button>
                    </div>
                </div>
            </form>

    </div>
</div>
@endsection()
