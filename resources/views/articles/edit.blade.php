@extends('layout')
@section('content')
<div id="wrapper">
    <div id="page" class="container">
            <h1 class="heading">Update article</h1>
            <form method="POST" action="{{route('articles.edit',$article)}}" >
                @csrf
                @method('PUT')
                <div class="field">
                    <label class="label" for="title">Title:</label>

                    <div class="control">
                        <input class="input" type="text"  name="title" id="title" value="{{$article->title}}"  >
                    </div>
                </div>

                <div class="field">
                    <label class="label" for="exerpt">Excerpt:</label>

                    <div class="control">
                    <textarea class="textarea" name="exerpt" id="exerpt" value="{{$article->exerpt}}" ></textarea>
                    </div>
                </div>

                <div class="field">
                    <label class="label" for="body">Body:</label>

                    <div class="control">
                        <textarea class="textarea" name="body"
                        id="body"

                        value="{{$article->body}}" ></textarea>
                    </div>
                </div>
                <div class="field is-grouped">
                    <div class="control">
                        <button class="button is-link" type="submit">Submit</button>
                    </div>
                </div>
            </form>
            <div>
                <h1>Delete:  {{$article->title}}</h1>
                <form method="POST" action="{{ route('articles.delete',$article)}}" >
                    @csrf
                    @method('DELETE')

                    <div class="field is-grouped">
                        <div class="control">
                            <button class="button is-danger" type="submit">Delete</button>
                        </div>
                    </div>
                </form>
            </div>

    </div>
</div>
@endsection()
