@extends('layout')
@section('content')
<div id="wrapper">
    <div id="page" class="container">

       <div id="content">
           @forelse ($articles as $article)
            <div class="title">
                  <h2><a href="{{$article->path()}}">
                     {{$article->title}}</a>
                </h2>
            </div>
            <div>
            <p>{{$article->body}}</p>
            </div>
           @empty
            <p>Not relevant article yet.</p>
           @endforelse
       </div>


        <div id="sidebar">
            <ul class="style1">

            @foreach($articles as $article)
                <li class="first">
                    <h3><a href="{{$article->path()}}">
                        {{ $article->title}}</a></h3>
                    <p>{{$article->body}}.</p>
                </li>
            @endforeach

            </ul>
        </div>

    </div>
</div>
@endsection()
