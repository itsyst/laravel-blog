{{-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Email</title>
</head>
<body>

    <h1>It works Again!</h1>
<p>It sounds like you want to hear about som kind of {{$topic}}</p>
</body>
</html> --}}
@component('mail::message')
# A Heading
Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sed, eos? Eos, ratione libero. Alias, tenetur.

- A list
- Goes
- Here

Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ex earum temporibus magnam dignissimos deleniti qui porro placeat voluptatibus voluptate at quisquam, quos tempore quasi reiciendis, perspiciatis consectetur? Distinctio ex laborum amet vitae sequi reprehenderit. Tempore beatae ex dolorem nisi, aperiam saepe, sit dignissimos accusantium repellat ab ducimus quia culpa sed!

@component('mail::button', ['url'=>'https://laracasts.com'])
Visit Laracasts
@endcomponent

// php artisan make:mail Contact --markdown=emails.contact
@endcomponent
