@extends('layout')

@section('content')
<div id="page" class="container">
    <div id="content" class="flex items-center justify-center h-full">
        <form method="POST" action="/contact" class="p-6 bg-white shadow rouded-md">
          @csrf
          <div>
            <label for="email">E-Mail Address</label>

            <input type="text" id="email" name="email" class="w-full px-2 py-1 text-sm border">
            @error('email')
          <div class="text-xs text-red-500">{{$message}}</div>
            @enderror
        </div>
              <button type="submit" class="w-full py-2 text-sm text-white bg-blue-500 rouded-full">
                Email Me
              </button>
              @if(session('message'))
                  <div class="mt-2 text-xs text-green-500">
                      {{session('message')}}
                  </div>
              @endif
        </form>
    </div>
</div>
@endsection
