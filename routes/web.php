<?php

use App\Http\Controllers\ArticlesController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/test', function () {
//     $name = request('name');

//     return view('test',['name' =>$name]);
// });

// Route::get('/test', function () {
//     $title = request('title');
//     return view('test', ['title' => $title]);
// });

// Route::get('/posts/{post}', function ($post) {

//     $posts = [
//         'my-first-post' => ' Hello this is my first post',
//         'my-second-post' => ' Hello this is my second post'
//     ];

//     if (!array_key_exists($post, $posts)) {
//         abort(404, 'Not Found! ');
//     }

//     return view('post', [
//         'post' => $posts[$post]
//     ]);
// });

// Route::get('/posts/{post}', 'PostsController@show');


Route::get('/', function () {
    return view('welcome');
    // return view('Home'); // use this to redirect to login page
});

Route::get('/about', function () {

    return view('about', [
        'articles' => App\Article::latest()->take(3)->get()
    ]);
});

Route::get('/contact','ContactController@show');
Route::post('/contact','ContactController@store');

Route::get('/articles', 'ArticlesController@index')->name('articles.index');
Route::post('/articles', 'ArticlesController@store');
Route::get('/articles/create', 'ArticlesController@create');
Route::get('/articles/{article}', 'ArticlesController@show')->name('articles.show');
Route::get('/articles/{article}/edit', 'ArticlesController@edit')->name('articles.edit');
Route::put('/articles/{article}', 'ArticlesController@update');
Route::delete('/articles/{article}/delete', 'ArticlesController@destroy')->name('articles.delete');

Route::get('/payments/create', 'PaymentsController@create')->middleware('auth');
Route::post('payments', 'PaymentsController@store')->middleware('auth');
Route::get('notifications', 'UserNotificationsController@show')->middleware('auth');

// Limit Access to Authorized Users
Route::get('conversations', 'ConversationsController@index')->middleware('auth');
Route::get('conversations/{conversation}', 'ConversationssController@show')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('Home');
