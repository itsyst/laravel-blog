<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    // public function getRouteKeyName()
    // {
    //     return 'title';
    // }

    protected $fillable = ['title', 'exerpt', 'body'];

    // protected $guarded = [];
    public function path()
    {
        return route('articles.show', $this);
    }


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class)->withTimestamps();
    }
}
