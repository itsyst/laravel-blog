<?php

namespace App\Http\Controllers;

use App\Events\ProductPurchased;
use App\Notifications\PaymentReceived;
use Faker\Provider\ar_SA\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class PaymentsController extends Controller
{
    public function create()
    {
        return view('payments.create');// resources/views/payments/create.blade.php
    }

    public function store()
    {
        // request()->user()->notify(new PaymentReceived()); // use this to notify just one user
        Notification::send(request()->user(), new PaymentReceived(900)); //to notify a collection of users here we hardcode the amount
        // Now we will store the notification to the database and also send it as an email

        //Core logic:
        //process the payment
        //unlock the purchase


        //Event:
        //ProductPurchased
        ProductPurchased::dispatch('toy');
        //event(new ProductPurchased('toy')) or use this it's the same

        //Listners
        //Side Effects:
        //notify the user about the payment
        //award achievements
        //send shareable coupon to user
    }
}
