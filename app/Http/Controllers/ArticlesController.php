<?php

namespace App\Http\Controllers;

use App\Article;
use App\Tag;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // die('Hello');
        if (request('tag')) {
            $articles = Tag::where('name', request('tag'))->firstOrFail()->articles;
            // return $articles;
        }
        else {
            $articles = Article::latest()->get();
        }
        return view('articles.index', ['articles' => $articles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('/articles/create', [
            'tags'=>Tag::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        // die('Hello');
        // request()->validate([
        //     'title' => 'required',
        //     'exerpt' => 'required',
        //     'body' => 'required'
        // ]);

        // $validatedArticle = request()->validate([
        //     'title' => 'required',
        //     'exerpt' => 'required',
        //     'body' => 'required'
        // ]);


        // $article = new Article( );

        // $article->title = request('title');
        // $article->exerpt = request('exerpt');
        // $article->body = request('body');
        // $article->save();

        // dd(request()->all());
        $this->ArticleValidated();

        $article = Article::create(request(['title','exerpt','body']));
        $article-> user_id = 1; // auth()->id
        $article->save();

        $article->tags()->attach(request('tags'));

        return redirect(route('articles.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        // dd($id);
        // $article = Article::findOrFail($id);
        return view('articles.show', ['article' => $article]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        return view('articles.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Article $article)
    {
        // request()->validate([

        //     'title' => 'required',
        //     'exerpt' => 'required',
        //     'body' => 'required'
        // ]);

        // Persist the edited resource
        Article::update($this->ArticleValidated());
        return redirect(route('articles.show', $article));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        return view('articles.edit', compact('article'));

        // Article::delete($this->ArticleValidated());
        //delete shoud not be called statically
        $article->delete();
        return redirect(route('articles.index'));
    }

    protected function ArticleValidated()
    {
        return request()->validate([

            'title' => 'required',
            'exerpt' => 'required',
            'body' => 'required',
            'tags'=>'exists:tags,id' // the tag must exists in the tags table with the id
        ]);
    }
}
